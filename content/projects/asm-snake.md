+++
date = "2017-02-03T11:08:55+01:00"
title = "Asm snake: a snake clone you can boot"

+++

In the summer of 2016, I decided that I wanted to write something in x86 assembly.
This was the first project I had done in assembly, so I decided to go with something
funny to stay focused. I have always seen snake as a minimal game, and a game that
would be fun to implement by myself, so that is what I did.

## Choices made
There are two important choices I made in the very start of the project.
\\
***The assembly code should not depend on any operating system specific features.***
This choice was made because I felt it would be more fun to code it all using BIOS
routines. The choice to run without an operating system meant that I had to code
the game in 16 bit [real mode](https://en.wikipedia.org/wiki/Real_mode), which wasn't
really a bit problem for me, since I don't use large values, or 32-bit or 64-bit specific
instructions. The way I would run the program was using either an emulator such as
[QEMU](http://wiki.qemu.org/Main_Page), or by copying it to a USB and booting from that.

To boot an executable, it needs two special bytes at offset 510 and 511. This means that
the if the binary is 512 bytes large (or small), it will fit completely in the boot sector!
This made me wonder if it would be possible to write such a small snake clone, and I wanted
to try.

***The resulting binary should only be 512 bytes.***
This one gave me some small challenges, but I managed to do it. Some of the "tricks" I used
were

* Use factor out jump statements
* Make assumptions about registers contents
* 7 bit strings
* AND to clear a byte

The first two things did not make the code easier to read, so I decided to comment most
of the code, to be able to modify it later.

7-bit strings are strings which do not end in a 0 byte, but in the the ascii character with
its high bit set. This saved me one byte per allocated string.

By using `AND [label], 0` instead of `MOV [label], 0` I saved some space.

There are many other small tricks used to get a smaller binary, and they can be found by
reading the [source](https://gitlab.com/pmikkelsen/asm_snake).
## The language
The assembler I used was [NASM](http://www.nasm.us/). I chose that one because it
has the nicest syntax. I have no experience with other assemblers.

## Features

The game currently runs like a normal snake game. This means is has the following features:

* Game controls (WADS)
* Moving snake with visible head
* Food with random positions
* A score counter
* Different game over messages
* Collision detection
* Ability to restart the game with a keypress

## Source
The source code for asm_snake can be found [here](https://gitlab.com/pmikkelsen/asm_snake).
It is licenced under the MIT license

## References
The main sources I used for developing this program were

* [Ralf Brown's Interrupt List](http://www.ctyme.com/rbrown.htm)
* [OSDev.org](http://wiki.osdev.org/Main_Page)
* [Paul Carters pc-asm book](http://pacman128.github.io/pcasm/)