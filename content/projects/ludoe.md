+++
title = "Ludoe: a ludo server in Erlang"
date = "2017-07-30T21:59:54Z"
+++

In the last two weeks, I have been a bit excited about yet another programming
language: [Erlang](www.erlang.org). Comming from a functional programming
background, the language itself was very easy for me to learn, and I could focus
on all the fun stuff instead ([OTP](https://gitlab.com/pmikkelsen/ludoe)).
This is my first project in the language, and it
is perhaps not the best code, but it works, sort of. It is a small server
that allows many players to connect via telnet, and then have a fun time playing
[ludo](https://en.wikipedia.org/wiki/Ludo_(board_game)). Every user
has the option to host their own game, or to join another game.

The code can be found [here](https://gitlab.com/pmikkelsen/ludoe).

As a note, the game does not implement all the rules of ludo, since that was
not really my goal with this project.

## instructions
* To host a game, type `host`
* To join a game, type `join`
* To roll the dice, type `roll`
* To select a piece to move, type a number
* To quit, type `quit`


## trying it
Right now (Feb. 2018) I have a demo of the program running at alaok.org, so
if you want to try it without installing it yourself, just type
`telnet alaok.org 31415` in your terminal (windows users, perhaps in your CMD,
I don't know).
