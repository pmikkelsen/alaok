+++
title = "About"
date = "2017-02-01T08:36:00+01:00"
+++

This page has some informations about me, and some information about this blog.
I will update this as needed. For information about projects I have done, please
see the [projects](/projects) page. The information on this page
will mostly be in list form.

## About me

* Age: 19
* Full name: Peter Mikkelsen
* E-mail: petermikkelsen10@gmail.com
* Operating system used: GuixSD (a GNU/Linux distribution)
* Text editor: Emacs
* Spoken and written languages: Danish and English
* Programming languages I know and sometimes use: C, Haskell, Java, Go, Guile,
  Idris, x86 assembly, Erlang
* Things I care about: Software freedom
* Things I am currently excited about: Elixir,
[Guix](https://www.gnu.org/software/guix/), Compilers

## About this blog
This blog is my way of writing down what I do. It will mostly be about software,
but maybe also about electronics. The code I write for this blog will always be licenced
under the [Expat (MIT)](https://directory.fsf.org/wiki/License:Expat) license, unless anything else is
noted.

The blog is produced by [hugo](https://gohugo.io/), and hosted on a Digital Ocean
VPS running GuixSD. The source
for this blog can be found [here](https://gitlab.com/pmikkelsen/alaok).

The name of this blog *alaok*, is koala spelled backwards. Why a koala? My girlfriend
drew it and I thought it would be a cute little mascot.
