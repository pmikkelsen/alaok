+++
title = "Moving to Digital Ocean"
date = 2018-02-17T00:33:57+01:00

+++

Hello again, nice to be back after a litte more than one year.
This time I am blogging to let you know that this blog is not hosted
on Amazon anymore, but on [Digital Ocean](https://www.digitalocean.com/).
The reason I moved away was that I also wanted to host other things than
some static web pages, so I thought it would make more sense just to have
a VPS with my favourite GNU/Linux distribution on it,
[GuixSD](https://www.gnu.org/software/guix/).

## Why GuixSD?
The reason I wanted to run on GuixSD is because I like the fact that it is
completely free software, and that I can configure the entire system from one
file. I know that debian is also free software except for the blobs in the linux
kernel, but since I am also using GuixSD on my laptop, it just made sense for me
to run it on the VPS too. There was of course the problem that Digital Ocean
didn't provide an option to run a VPS with GuixSD, but I followed some steps
that I found online, that allowed me to install GuixSD on a running Debian
system, and then delete the remaining debian bits. One day I might write a blog
post about how I did it in details.

## Why Digital Ocean?
I went ahead an used Digital Ocean because they allow me to run the cheapest VPS
by default, and then scaling up to a more beefy one for short periods if I need
to, which I do when Guix itself needs to be updated. When scaling up, only the
CPU and the working memory is updated, and therefor there is no problem scaling
down again.

## Final notes
Even though I am not using Amazon anymore, the domain is still registered there,
but I now use a certificate from Let's Encrypt instead of one from Amazon.