+++
date = "2017-02-03T17:19:48+01:00"
title = "The setup of this blog"

+++

Since this is the first blog/website I have that is not hosted on
github pages, I have decided to write a little blog post about how it
is hosted and generated.

## Static site
First of all, this blog is completely static. That means there is no fancy
web framework or content management system, but only plan HTML/CSS/JavaScript.
I chose this approach since I feel like a blog doesn't need anything else than
static contents. There are no logins and no comment sections (not yet at least).

I write all my contents in the markdown format, and generate a static page using
the totally awesome static site generator, [hugo](https://gohugo.io). It was really
easy to setup hugo, and I followed roughly these steps:

1. Run `hugo new site alaok`
2. `cd alaok`
3. Edit `config.toml` to [the settings I needed](https://gitlab.com/pmikkelsen/alaok/blob/master/config.toml)
4. Download a theme into the `themes` folder (I choose [Cocoa-EH](https://themes.gohugo.io/cocoa-eh-hugo-theme/))
5. Generate some empty pages with `hugo new file`, where `file` is something like `blog/welcome.md`
6. Test with `hugo server`, and opening `localhost:1313` with my browser.
7. Upload it to [gitlab.com](https://gitlab.com/pmikkelsen/alaok)

To generate the contents, I just run `hugo`, and everything that needs to be copied
to a server will be in the `public` folder. The main reasons I chose hugo instead of other
site generators are:

1. It produces the content ***really*** fast.
2. It has automatic reloading, so that when I edit some contents and `hugo server`
is running, my browser will automatically refresh.
With that in place, lets move on to how I actually host my site.

## Hosting
The files are hosted on [Amazon S3](https://aws.amazon.com/s3), and are distributed around the globe
by [Amazon CloudFront](https://aws.amazon.com/cloudfront). The domain name I have is
registered at [Amazon route 53](https://aws.amazon.com/route53). Because everything
is a part of the amazon web services, it was really easy for me to setup the page.
I used the [s3_website](https://github.com/laurilehmijoki/s3_website) program to set it up, and I still use it to push changes to
the servers. For more information about s3_website, please see their github repo.

## HTTPS
The one thing I noticed when the website was setup was, that it ran over plain HTTP.
I didn't want that, since I believe that what can be encrypted should be, so I
looked into the options I had. The first option I found was to use [Let's Encrypt]
(https://letsencrypt.org/), to get a free certificate and use that. For a guide
to how I did it, please see [this](https://medium.com/@richardkall/setup-lets-encrypt-ssl-certificate-on-amazon-cloudfront-b217669987b2), since it is what I followed.

## Conclusion
The overall setup and hosting of this blog has gone well, and I like the way
it is now. In the future I may make my own theme, but I am not quite sure yet, since
it is a big task and I don't see a need right now.

The fact that I use AWS to host my stuff is still all new to me, so I don't know
if it will be a good experience forever. If not, I will find somewhere else to host my
blog and transfer it over there. It should be pretty straightforward since all my
configuration files and content are stored at gitlab.