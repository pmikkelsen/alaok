+++
date = "2017-02-01T12:12:02+01:00"
title = "Welcome"

+++

Welcome to my new blog! Previously I had another blog, but I shut it down because I didn't feel like it was good. Now I am trying again, and this time I hope to have the blog a little longer. In one of the following days, I will post about how this website is build and hosted.

If you have any questions, please email me. Note that I have chosen not to enable comments, and that is because I don't feel like they are worth it right now. If you want me to add them, please email me.
