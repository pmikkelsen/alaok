+++
title = "Home"
date = "2017-02-01T08:34:16+01:00"

+++

Welcome to my site where I write about computer stuff.
If you have any suggestions about projects or blogposts,
please contact me.

Looking for my PGP keys? Look no further than [here](keys).
Just looking for contact information? See the [about](about) page.
